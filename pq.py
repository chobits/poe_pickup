from heapq import heappush, heappop
import itertools

REMOVED = '<removed-task>'      # placeholder for a removed task

class priority_queue:
    def __init__(self):

        self.pq = []                         # list of entries arranged in a heap
        self.entry_finder = {}               # mapping of tasks to entries
        self.counter = itertools.count()     # unique sequence count

    def add(self, task, priority=0):
        'Add a new task or update the priority of an existing task'
        if task in self.entry_finder:
            self.remove_task(task)
        count = next(self.counter)
        entry = [priority, count, task]
        self.entry_finder[task] = entry
        heappush(self.pq, entry)
    
    def remove(self, task):
        'Mark an existing task as REMOVED.  Raise KeyError if not found.'
        entry = self.entry_finder.pop(task)
        entry[-1] = REMOVED

    def qsize(self):
        return len(self.entry_finder) 
    
    def pop(self, return_priority=False):
        'Remove and return the lowest priority task. Raise KeyError if empty.'
        while self.pq:
            priority, count, task = heappop(self.pq)
            if task is not REMOVED:
                del self.entry_finder[task]
                if return_priority:
                    return task, priority
                else:
                    return task
        raise KeyError('pop from an empty priority queue')

def test():
    q = priority_queue()
    q.add(("1", 1), 1)
    q.add(("2", 1), 1)
    q.add(("3", 1), 1)
    q.add(("4", 1), 1)
    q.add(("first", 1), 0)
    print(q.pop())
    print(q.pop())
    q.remove(("3", 1))
    print(q.pop())
    print(q.pop())

if __name__ == '__main__':
    test()
