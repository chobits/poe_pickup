import csv

class data:
    def __init__(self):
        self.path = "./itemlist.csv"
        self.fieldnames = ["url", "name"]
        self.load()

    def load(self):
        self.itemlist = []
        try:
            f = open(self.path)
            rd = csv.DictReader(f, fieldnames=self.fieldnames, delimiter='\t', quotechar='|')
            for row in rd:
                self.itemlist.append(row)
        except:
            import sys
            e = sys.exc_info()[0]
            print ("+ error: ", e)
            pass

    def save(self):
        with open(self.path, "w") as f:
            w = csv.DictWriter(f, fieldnames=self.fieldnames, delimiter='\t', quotechar='|')
            w.writerows(self.itemlist)

    def insert(self, row):
        self.itemlist.append(row)
        self.save()

    def remove(self, url, name):
        for i, row in enumerate(self.itemlist):
            if url == row["url"] and name == row["name"]:
                del self.itemlist[i]
                break
        self.save()

    def show(self):
        print("+ show db:")
        for row in self.itemlist:
            print(" + name:", row["name"])
            print(" + url:", row["url"])

    def test_init(self):
        import random
        self.itemlist = [
            { "url" : 'https://www.pathofexile.com/trade/search/Ultimatum/lGKVq82cV', "name" : "first\",of war support/anomalous/iv15+" + str(random.random())},
            { "url" : 'https://www.pathofexile.com/trade/search/Ultimatum/bE7pzLLFL', "name" : "\tresistance+damage jew"}
        ]
        self.save()
        print("+ save:", self.itemlist)
        self.load()

        print("+ load:", self.itemlist)
        for row in self.itemlist:
            print("name:", row["name"])


def test():
    d = data()
    print (d.itemlist)
    d.test_init()


#test()
