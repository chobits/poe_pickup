import requests
import re
import time
import json
import pprint

rlog = []

def rdebug_log():
    print("--- request log ---")
    for i, e in enumerate(rlog):
        print("[%3s] %s" %(i, e))


def rdebug(r):
    print("> %s %s" %(r.request.method, r.url))
    print("> return: %s %s" %(r.status_code, r.reason))
    print("--- headers ---")
    for k,v in r.headers.items():
        print("%s：%s" %(k, v))
    #print("--- response ---")
    #print(r.text)
    print("\n\n")
    t = time.strftime(" %m/%d %H:%M:%S", time.localtime())
    rlog.append("%s: %s %s -> %s" %(t, r.request.method, r.url, r.status_code))

# url 'https://www.pathofexile.com/trade/search/Ultimatum/lGKVq82cV'
#                                                         ^^ hash id
def get_item_info(url):
    us = url.split("/")
    league = us[-2]
    qid = us[-1]
    print("league:%s, qid:%s" %(league, qid))

    r = rget(url)

    # test case:
    #class _r:
    #    text = ""
    #r = _r()
    #r.text = 'require(["main"], function(){require(["trade"], function(t){    t({"tab":"search","leagues":[{"id":"Expedition","text":"Expedition"},{"id":"Hardcore Expedition","text":"Hardcore Expedition"},{"id":"Standard","text":"Standard"},{"id":"Hardcore","text":"Hardcore"}],"news":{"trade_news":{"id":1044,"url":"https:\/\/www.pathofexile.com\/forum\/view-thread\/3136724","image":"https:\/\/web.poecdn.com\/public\/news\/2021-06-08\/3.15TimelineFullNews.jpg"}},"league":"Expedition","state":{"type":"Fist of War Support","stats":[{"type":"and","filters":[],"disabled":false}],"status":"onlineleague","filters":{"misc_filters":{"filters":{"gem_level":{"min":15,"max":null},"gem_alternate_quality":{"option":"1"}},"disabled":false}}},"loggedIn":false});});});'
    pattern1 = 'require(["main"], function(){require(["trade"], function(t){'
    patternL = '"leagues":(\[.*\])'
    patternS = '"state":{(.*)},"loggedIn"'
    pattern = '.*%s.*%s.*%s' % (re.escape(pattern1), patternL, patternS)
    print ("--- pattern ---")
    print (pattern)
    # print(pattern)
    items = re.findall(pattern, r.text)

    _leagues = json.loads(items[0][0])
    leagues = [ i["text"] for i in _leagues ]
    status = items[0][1]

    print ("\n--- find state--")
    print (status)

    print ("\n--- find league ---")
    print (leagues)

    if league not in leagues:
        for l in leagues:
            ll = l.lower()
            if 'standard' not in ll or 'hardcore' not in ll:
                print ("\n --- league fixed ---")
                print ("%s -> %s" %(league, l))
                league = l
                break

    return status, league, qid
 

def rget(url):
    headers = {
            'authority': 'www.pathofexile.com' ,
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36' ,
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' ,
            'accept-language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7,zh-TW;q=0.6'
    }
    r = requests.get(url, headers=headers)
    rdebug(r)
    return r


def status_to_postdata(status):
    # try to POST data
    print ("--- post ---")
    payload = '{"query":{%s}, "sort":{"price":"asc"}}' % status
    return payload


def rpost_itemlist(url, payload):
    if not url:
        url = 'https://www.pathofexile.com/api/trade/search/Ultimatum'

    headers = {
            'authority': 'www.pathofexile.com' ,
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36' ,
            'accept': '*/*' ,
            'accept-language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7,zh-TW;q=0.6',
            'content-type' : 'application/json'
    }

    r = requests.post(url, headers=headers, data=payload)
    rdebug(r)
    # X-Rate-Limit-Policy: trade-search-request-limit
    # X-Rate-Limit-Rules: Ip
    # X-Rate-Limit-Ip: 5:10:60,15:60:300,30:300:1800
    #                  5 requests per 10 seconds; throttled by 60 seconds
    # X-Rate-Limit-Ip-State: 1:10:0,1:60:0,2:300:945
    # Retry-After: 945

    return r

def rget_itemlist(ourl, json_text, n, qid=None, url=None):
    if url:
        print ("url:", url)
        return rget(url)

    resp = json.loads(json_text)

    if len(resp["result"]) == 0 or resp["total"] == 0:
        return None
    # print(" qid: ", resp["id"])

    q = ""

    if qid:
        n = 10  # -- maximum --
        q = "?query=%s" % qid

    qpath = ",".join(resp["result"][0:n])

    if "poe.game.qq.com" in ourl:
        url = 'https://poe.game.qq.com/api/trade/fetch/%s%s' % (qpath, q)
    else:
        url = 'https://www.pathofexile.com/api/trade/fetch/%s%s' % (qpath, q)

    return rget(url)

def get_item_result_list(url, payload=None):
    status, league, qid = get_item_info(url)

    if not payload:
        payload = status_to_postdata(status)

    print ("--- post payload ----")
    print (payload)

    if "poe.game.qq.com" in url:
        post_url = 'https://poe.game.qq.com/api/trade/search/%s' %league
    else:
        post_url = 'https://www.pathofexile.com/api/trade/search/%s' %league

    r = rpost_itemlist(post_url, payload)

    r = rget_itemlist(url, r.text, 5)
    if not r:
        return None
    t = json.loads(r.text)
    item_result_list = t["result"]

    for item in item_result_list:
        print ("--- item:")
        pprint.pprint (item["listing"]["price"])

    return item_result_list

# get search api of web from POST request
def post_to_search_url(league, payload):
    post_url = 'https://www.pathofexile.com/api/trade/search/%s' %league
    r = rpost_itemlist(post_url, payload)
    resp = json.loads(r.text)

    if "id" in resp:
        return "https://www.pathofexile.com/trade/search/%s/%s" %(league, resp["id"])

    return None

# for test
def test():
    url = 'https://www.pathofexile.com/trade/search/Ultimatum/lGKVq82cV'
    url = 'https://www.pathofexile.com/trade/search/Ultimatum/bE7pzLLFL'
    url = 'https://www.pathofexile.com/trade/search/Ultimatum/k97Z0Dwf5'
    url = 'https://www.pathofexile.com/trade/search/Ultimatum/yMezZDeHR'
    url = 'https://www.pathofexile.com/trade/search/Scourge/Qn0BDvOSw'

    get_item_result_list(url)

if __name__ == '__main__':
    test()
