from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from multiprocessing import Process, Queue
import webbrowser
import random

import pq
import data
import time
import poe_api

def task_proc(task_queue, result_queue):

    working_queue = pq.priority_queue()

    while True:
        time.sleep(1)
        ts = int(time.time())

        # get task from PROCESS queue
        while task_queue.qsize() > 0:
            task = task_queue.get()
            if task[0] == "update_all":
                itemlist = task[1]
                for item in itemlist:
                    working_queue.add(item, priority=1)
            elif task[0] == "add":
                item = task[1]
                working_queue.add(item, priority=0) # highest priority
            elif task[0] == "remove":
                item = task[1]
                working_queue.remove(item)

        # get & run task from working queue
        if working_queue.qsize() > 0:
            (url, name), run_ts = working_queue.pop(return_priority=True)

            if ts < run_ts:
                print ("+ %s < %s, run task later" %(ts, run_ts))
                working_queue.add((url,name), priority=run_ts)
                continue

            try:
                item_result_list = poe_api.get_item_result_list(url)
                if item_result_list:
                    lowest_price = item_result_list[0]["listing"]["price"]
                    lpstr = "%s %s" %(lowest_price["amount"], lowest_price["currency"])
                else:
                    lpstr = "[item not found]"
            except Exception as e:
                poe_api.rdebug_log()
                lpstr = "Error: %s" % e

            # {'amount': 280, 'currency': 'chaos', 'type': '~price'}
            t = time.strftime(" %m/%d %H:%M:%S", time.localtime())

            result_queue.put({"url":url, "name":name, "price":lpstr, "time":t})

            # re-run this task after 0.5~1.5 minute
            run_ts = int(time.time()) + random.randint(30, 90)
            working_queue.add((url,name), priority=run_ts)

            # status code: 429
            # X-Rate-Limit-Ip：5:10:60,15:60:300,30:300:1800
            # X-Rate-Limit-Ip-State：2:10:0,7:60:0,31:300:1800
            #                                      ^^^^^^^^^^^
            #   31 requests in latest 300s exceeds 30r/300s limit
            #   that means no more than 1 reqeust in 10s
            time.sleep(11)

class Application(Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.title('Poe Shopping Cart')
#        self.geometry('1000x600')

        self.db = data.data()

        root = self

        button_add = Button(root, text='Add', command=self.button_add, width=8)
        button_add.pack()

        #button_show = Button(root, text='Show items  ', command=self.show_items)
        #button_show.pack()

        self.task = None
        self.start_button = Button(root, text='Start', command=self.start_task, width=8)
        self.start_button.pack()

        self.stop_button = Button(root, text='Stop', command=self.stop_task, width=8)
        self.stop_button.configure(state='disabled')
        self.stop_button.pack()

        self.statuslist = {}
        for item in self.db.itemlist:
            self.statuslist[(item["url"], item["name"])] = { "price":"", "time":"", "tvhash":""}

        self.show_status()

    def start_task(self):
        assert(self.task is None)
        self.task_queue = Queue()
        self.result_queue = Queue()
        self.task = Process(target=task_proc, args=(self.task_queue, self.result_queue))
        self.start_button.configure(state='disabled')
        self.stop_button.configure(state='normal')

        task_list = list(self.statuslist.keys())
        self.task_queue.put(("update_all", task_list))

        self.after(1000, self.update_status)
        self.task.start()

    def stop_task(self):
        assert(self.task is not None)
        print("+ kill sub process")
        self.task.terminate()
        self.task.join()
        self.task = None
        self.task_queue = None
        self.result_queue = None
        self.start_button.configure(state='normal')
        self.stop_button.configure(state='disabled')

    def add_task(self, key):
        if self.task_queue:
            self.task_queue.put(("add", key))

    def remove_task(self, key):
        if self.task_queue:
            self.task_queue.put(("remove", key))

    def update_status(self):
        if not self.task:
            print ("+ no sub process, cannot update status")
            return

        if self.result_queue.qsize():
            item = self.result_queue.get(0)
            self.update_item_status(item)
            print("+ get result [%s] from queue" % item)
        else:
            print("+ not get")

        #   if exit code is None - process is on the run and we should re-schedule check
        if self.task.exitcode is None:
            self.after(1000, self.update_status)
        #   things are executed
        else:
            print("+ exit sub process")
            self.task.join()
            self.task = None
            self.task_queue = None
            self.result_queue = None
            self.start_button.configure(state='normal')
            self.stop_button.configure(state='disabled')

    def update_item_status(self, item):
        if (item["url"], item["name"]) not in self.statuslist:
            print("+ not found %s, maybe deleted" %item)
            return
        iid = self.statuslist[(item["url"], item["name"])]["tvhash"]
        v = (iid, item["url"], item["name"], item["price"], item["time"])
        self.statustable.item(iid, values=v)

    def show_status(self):

        try:
            # TODO::::
            if self.statustable.state() == "normal":
                self.statustable.destroy()
        except:
            pass

        frame = Frame(self)
        frame.pack(pady=20)
        tv = ttk.Treeview(frame, columns=(0, 1, 2, 3, 4), show='headings')
        tv.pack(side=LEFT)

        tv.heading(0, text="")
        tv.column(0, minwidth=0, anchor=E, width=30)
        tv.heading(1, text="url")
        tv.column(1, minwidth=0, anchor=E)
        tv.heading(2, text="name")
        #tv.column(2, minwidth=0, anchor=E)
        tv.heading(3, text="lowest price")
        tv.column(3, minwidth=0, anchor=E)
        tv.heading(4, text="time")
        tv.column(4, minwidth=0, anchor=E, width=130)


        sb = Scrollbar(frame, orient=VERTICAL)
        sb.pack(side=RIGHT, fill=Y)

        tv.config(yscrollcommand=sb.set)
        sb.config(command=tv.yview)

        i = 0
        for (url,name), item in self.statuslist.items():
            v = (i, url, name, item["price"], item["time"])
            # index = position
            # iid = item id
            # return iid
            iid = tv.insert(parent='', index=i, iid=i, values=v)
            item["tvhash"] = iid
            print (iid, type(iid))
            i = i + 1

        tv.bind("<Double-1>", self.status_double_click)
        tv.bind("<Button-3>", self.status_right_click)

        style = ttk.Style()
        style.theme_use("default")
        style.map("Treeview")


        # statustable menu
        menu = Menu(tv, tearoff=0)
        menu.add_command(label="Remove", command=self.menu_remove)
        menu.add_command(label="Update Now")
        menu.add_separator()
        menu.add_command(label="Add")

        tv.menu = menu


        self.statustable = tv

    def menu_remove(self):
        if self.statustable.selection() == ():
            print("+ no item selected")
            return
        iid = self.statustable.selection()[0]
        v = self.statustable.item(iid, "values")
        url = v[1]
        name = v[2]
        self.db.remove(url, name)
        self.statustable.delete(iid)
        del self.statuslist[(url,name)]

    def status_right_click(self, event):
        print("+ clicked on Event:", event)
        region = self.statustable.identify("region", event.x, event.y)
        if region == "heading":
            print("+ click tv heading, nothing happened.")
            return

        #self.statustable.set(self.statustable.identify_row(event.y))
        iid = self.statustable.identify_row(event.y)
        self.statustable.selection_set(iid)

        self.statustable.menu.selection = iid
        self.statustable.menu.post(event.x_root, event.y_root)

    def status_double_click(self, event):
        iid = self.statustable.identify_row(event.y)
        self.statustable.selection_set(iid)
        print("+ clicked on Event:", event)
        region = self.statustable.identify("region", event.x, event.y)
        if region == "heading" or region == "nothing":
            print("+ click [%s], nothing happened." % region)
            return
        s = self.statustable.selection()
        print("+ clicked on: selection(): ", s)
        iid = s[0]
        v = self.statustable.item(iid, "values")
        url = v[1]
        print("+ clicked on:", url)
        webbrowser.open_new(url)

    #def show_items(self, event=None):
    #    self.db.show()

    def button_add(self, event=None):
        try:
            if self.new_window.state() == "normal":
                self.new_window.focus()
                return
        except:
            pass

        new_window = Toplevel(self)
        new_window.title("add new item")
        new_window.geometry("600x200")
        self.new_window = new_window
        # A Label widget to show in toplevel

        Label(new_window, text="URL ").grid(row=0)
        Label(new_window, text="Name").grid(row=1)

        e1 = Entry(new_window, width=60)
        e2 = Entry(new_window, width=60)

        e1.grid(row=0, column=1)
        e2.grid(row=1, column=1)

        button = Button(new_window, text="add",
                        command= lambda: self.button_add_add(e1, e2))
        button.grid(row=2)

    def button_add_add(self, e1, e2):
        url = e1.get().strip()
        name = e2.get()

        self.new_window.destroy()

        if (url,name) in self.statuslist:
            messagebox.showinfo("", "Duplicated <URL,NAME>")
            return

        self.db.insert({"url":url, "name":name})
        i = len(self.statuslist)
        v = (i, url, name, "", "")
        iid = self.statustable.insert(parent='', index=i, iid=i, values=v)
        self.statuslist[(url,name)] = { "price":"", "time":"", "tvhash":iid}
        self.add_task((url,name))


if __name__ == '__main__':
    app = Application()
    app.mainloop()
