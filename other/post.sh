curl 'https://www.pathofexile.com/api/trade/search/Ultimatum' \
  -H 'authority: www.pathofexile.com' \
  -H 'accept: */*' \
  -H 'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36' \
  -H 'content-type: application/json' \
  --data-raw '{"query":{"status":"onlineleague","type":"Fist of War Support","stats":[{"type":"and","filters":[],"disabled":false}],"filters":{"misc_filters":{"filters":{"gem_level":{"min":15,"max":null},"gem_alternate_quality":{"option":"1"}},"disabled":false}}},"sort":{"price":"asc"}}' 

  #-H 'x-requested-with: XMLHttpRequest' \
  #--data-raw '{"query":{"status":{"option":"onlineleague"},"type":"Fist of War Support","stats":[{"type":"and","filters":[],"disabled":false}],"filters":{"misc_filters":{"filters":{"gem_level":{"min":15,"max":null},"gem_alternate_quality":{"option":"1"}},"disabled":false}}},"sort":{"price":"asc"}}' \
